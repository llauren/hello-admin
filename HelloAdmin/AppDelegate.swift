//
//  AppDelegate.swift
//  HelloAdmin
//
//  Created by Robin Laurén on 12-12-2017.
//  Copyright © 2017 Robin Laurén. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

