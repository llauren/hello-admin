//
//  ViewController.swift
//  HelloAdmin
//
//  Created by Robin Laurén on 12-12-2017.
//  Copyright © 2017 Robin Laurén. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    @IBOutlet weak var osVersionLabel: NSTextField!
    
    @IBOutlet var connectionsTextView: NSTextView!
    
    @IBAction func connectionsButtonPushed(_ sender: Any) {

        var ChainedCommandResult: String?
        
        let netstatCommand = CliCommand(launchPath: "/usr/sbin/netstat", arguments: ["-an"])
        let grepCommand = CliCommand(launchPath: "/usr/bin/grep", arguments: ["ESTABLISHED"])

        let chainedCommand = TaskHelper(commands: [netstatCommand, grepCommand])
        
        // execute!
        do {
            ChainedCommandResult = try chainedCommand.execute()
        } catch _ {
            print("AAAAGH! FAILURE!")
        }
        
        print(ChainedCommandResult!)
        
        connectionsTextView.string = ChainedCommandResult!
    }
    
    func getOsVersion() -> String {
        let osVersion = ProcessInfo.processInfo.operatingSystemVersionString
        print(osVersion)
        
        //osVersionLabel.stringValue = osVersion
        return(osVersion)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // getOsVersion()
        osVersionLabel.stringValue = getOsVersion()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

